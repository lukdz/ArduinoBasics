int kit = 14;
int val;
const int  NL = 7;
int PIN[NL] = {9,8,7,6,5,4,3};

int p = 0;
int tab[25][NL] = {{1,1,1,1,1,1,1},//H
                  {0,0,0,1,0,0,0},
                  {0,0,0,1,0,0,0},
                  {1,1,1,1,1,1,1},
                  {0,0,0,0,0,0,0},
                  {1,1,1,1,1,1,1},//E
                  {1,0,0,1,0,0,1},
                  {1,0,0,1,0,0,1},
                  {1,0,0,0,0,0,1},
                  {0,0,0,0,0,0,0},
                  {1,1,1,1,1,1,1},//L
                  {1,0,0,0,0,0,0},
                  {1,0,0,0,0,0,0},
                  {1,0,0,0,0,0,0},
                  {0,0,0,0,0,0,0},
                  {1,1,1,1,1,1,1},//L
                  {1,0,0,0,0,0,0},
                  {1,0,0,0,0,0,0},
                  {1,0,0,0,0,0,0},
                  {0,0,0,0,0,0,0},
                  {0,1,1,1,1,1,0},//O
                  {1,0,0,0,0,0,1},
                  {1,0,0,0,0,0,1},
                  {0,1,1,1,1,1,0},
                  {0,0,0,0,0,0,0}};


void setup() {
  Serial.begin(9600);
  for(int i=0; i<NL; i++){
    pinMode(PIN[i], OUTPUT);
  }
  val = analogRead(kit);
}

void loop() {
  if(abs(analogRead(kit)-val) > 5){
    for(int i=0; i<NL; i++){
      digitalWrite(PIN[i], LOW);
    }
    p=0;
  }
  if(abs(analogRead(kit)-val) < 5 && p<25){
    for(int i=0; i<NL; i++){
      if(tab[p][i] == 1){
        digitalWrite(PIN[i], HIGH);
      }
      else{
        digitalWrite(PIN[i], LOW);
      }
    }
    p = p+1;  
    delayMicroseconds(1500);
  }
}
