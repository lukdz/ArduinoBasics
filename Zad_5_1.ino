int tab [100] = {0}; //do zapisu stanu przycisku
int i = 0; //do przechodzenia po tabeli stanów przycisku

void setup() {
  pinMode(8, OUTPUT); //Dioda jako wyjście
  pinMode(7, INPUT_PULLUP); //Przycisk jako wejście
}
 
void loop(){  
  if (tab [i] == 1) { //Jeśli przycisk był wciśnięty
    digitalWrite(8, HIGH); //Włącz diodę
  } else { //Jeśli przycisk nie był jest wciśnięty
    digitalWrite(8, LOW); //Wyłącz diodę
  }
    
  if (digitalRead(7) == LOW) { //Jeśli przycisk jest wciśnięty
    tab [i] = 1;
  } else { //Jeśli przycisk nie jest wciśnięty)
    tab [i] = 0;
  }

  i=(i+1)%100; //przejście do następnego miejsca w tabeli

  delayMicroseconds(9700);
  
}
