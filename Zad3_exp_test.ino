void setup() {
  pinMode(9, OUTPUT); //Dioda jako wyjście
  pinMode(10, OUTPUT); //Dioda jako wyjście
  pinMode(11, OUTPUT); //Dioda jako wyjście

  analogWrite(9, 255); //Wyłączenie diody R
  analogWrite(10, 255); //Wyłączenie diody G
  analogWrite(11, 255); //Wyłączenie diody B
  
  randomSeed(analogRead(A0));
}
 
void loop() {
  float R,G,B;
  
  do{
    R = random(256);   //Odczytujemy wartość "losową"
    G = random(256);   //Odczytujemy wartość "losową"
    B = random(256);   //Odczytujemy wartość "losową"
  }while( (R+G+B) < 100 );      //Do momentu, gfzy uzyskamy jasny kolor
  
  kolor(R,G,B);  //przekazanie wylosowanego koloru do funkcji migania
}

void kolor(int R,int G, int B){
  float W;
  for(float x=-3.14159; x<=3.14159; x=x+0.01){ //pętla migania
    W = pow(2.71828,cos(x));
    W = W-0.36788;
    W = W/2.3504;
    RGB(R*W,G*W,B*W); //przekazanie do funkcji wyświetlajćej kolor
  }
}

void RGB(int R, int G, int B){
  analogWrite(9, 255-R);    //Włączenie diody
  analogWrite(10, 255-G);   //Włączenie diody
  analogWrite(11, 255-B);   //Włączenie diody

  delay(8);
}

