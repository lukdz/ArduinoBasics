#include <stdio.h>
#include <math.h>

int main(void){
    int min=5, max=1024;
    printf("Podaj min max: ");
    //scanf("%d %d", min max);
    if(max>491520){
        printf("Za duza gorny zakres");
        return 0;
    }
    for(int l=min; l<=max; l++){
        if((l&15)==15 ||
           (l&30)==30 ||
           (l&60)==60 ||
           (l&120)==120 ||
           (l&240)==240 ||
           (l&480)==480 ||
           (l&960)==960 ||
           (l&1920)==1920 ||
           (l&3840)==3840 ||
           (l&7680)==7680 ||
           (l&15360)==15360 ||
           (l&30720)==30720 ||
           (l&61440)==61440 ||
           (l&122880)==122880 ||
           (l&245760)==245760 ||
           (l&491520)==491520){
                printf("%d ", l);
        }
    }
    return 0;
 }
